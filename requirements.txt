git+https://github.com/xivo-pbx/xivo-lib-python.git  # from xivo-dao
git+https://github.com/xivo-pbx/xivo-dao.git
jinja2==2.7
netaddr==0.7.12
pyyaml==3.11  # from xivo-lib-python
sqlalchemy==0.9.8  # from xivo-dao
